# Flash HDD LED on Linux <!-- Metadata: type: Outline; tags: hdd,linux,led; created: 2021-04-08 20:46:23; reads: 3; read: 2021-04-08 20:47:51; revision: 3; modified: 2021-04-08 20:47:51; importance: 0/5; urgency: 0/5; -->
ledctl (from package ledmon) is really the way to go with this.

`ledctl locate=/dev/disk/by-id/[drive-id]`

or

`ledctl locate=/dev/sda`

will illuminate the drive fail light on your chassis for the specified drive. I provided two examples to illustrate that it doesn't matter HOW you identify the drive. You can use serial, name, etc... Whatever information is available to you can be used. The drives are referenced multiple ways under the /dev/ and /dev/disk/ path.

To turn the light back off, just execute it again, changing locate to locate_off like so:

`ledctl locate_off=/dev/sda`
# Note <!-- Metadata: type: Note; created: 2021-04-08 20:46:23; reads: 1; read: 2021-04-08 20:46:23; revision: 1; modified: 2021-04-08 20:46:23; -->

