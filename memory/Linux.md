# Linux <!-- Metadata: type: Outline; created: 2022-02-16 19:49:37; reads: 44; read: 2023-02-01 15:19:15; revision: 41; modified: 2023-02-01 15:19:15; importance: 0/5; urgency: 0/5; -->
# Как победить no matching key exchange method found <!-- Metadata: type: Note; tags: linux,ssh; created: 2022-02-16 19:50:50; reads: 6; read: 2022-12-14 13:33:36; revision: 3; modified: 2022-02-16 20:08:12; -->
На некоторых устройствах при подключении по ssh можно словить следующее сообщение:

`Unable to negotiate with host: no matching key exchange method found. Their offer: diffie-hellman-group1-sha1`

Лечится это добавлением алгоритма Diffie Hellman

`$ ssh -oKexAlgorithms=+diffie-hellman-group1-sha1 user@host`

Но потом мы можем получить следующее:

`Unable to negotiate with host port 22: no matching cipher found. Their offer: aes128-cbc,3des-cbc,aes192-cbc,aes256-cbc`

Проверим, какие типы шифрования поддерживает наш ssh клиент

```
$ ssh -Q cipher
3des-cbc
aes128-cbc
aes192-cbc
aes256-cbc
rijndael-cbc@lysator.liu.se
aes128-ctr
aes192-ctr
aes256-ctr
aes128-gcm@openssh.com
aes256-gcm@openssh.com
```

Видим, что можем использовать, например, aes256-cbc

`$ ssh -c aes256-cbc -oKexAlgorithms=+diffie-hellman-group1-sha1 user@host`

Готово. Там мы подключились к нашему устройству через ssh.

# ключ ssh в окнфигурации <!-- Metadata: type: Note; created: 2023-02-01 15:16:54; reads: 3; read: 2023-02-01 15:19:15; revision: 2; modified: 2023-02-01 15:19:15; -->
Чтобы git не спрашивал пароль создать файл `~/.ssh/config`:
```
Host *
  AddKeysToAgent yes
  IdentityFile ~/.ssh/id_orangepione
```
# cabin4 rocky linux 8.7 <!-- Metadata: type: Note; created: 2022-02-16 19:49:37; reads: 24; read: 2023-02-01 15:16:08; revision: 17; modified: 2022-12-21 18:02:15; -->
Установка KDE
```
sudo dnf update
sudo dnf install epel-release
sudo dnf config-manager --set-enabled powertools
sudo dnf groupinstall -y "KDE Plasma Workspaces"
sudo systemctl set-default graphical.target
```
Установка firefox
```
sudo dnf install -y firefox
```
Установка nomachine
```
wget https://download.nomachine.com/download/8.2/Linux/nomachine_8.2.3_4_x86_64.rpm
sudo dnf install ./nomachine_8.2.3_4_x86_64.rpm 
```
Копирование файлов
```
rsync -aH super@cabin2:/home/super /home
```
Установка средств разработки
```
sudo dnf install -y qt-creator qt5-qtserialport-devel qt5-qtserialbus-devel qt5-qtbase-private-devel cppzmq-devel lapack-devel
sudo dnf groupinstall -y "Development Tools"
```
Сборка qt mqtt
```
git clone https://github.com/qt/qtmqtt.git --branch 5.15.2
cd qtmqtt/
qmake-qt5
make
make install
```
Установка SDK сканера Zebra. Со страницы https://www.zebra.com/us/en/support-downloads/software/developer-tools/scanner-sdk-for-linux.html скачать SDK_for_Linux_v4.4.1-30_RPM_Packages_x86_64bit_C11.zip
```
unzip SDK_for_Linux_v4.4.1-30_RPM_Packages_x86_64bit_C11.zip
cd SDK_for_Linux_v4.4.1-30_RPM_Packages_x86_64bit_C11/
sudo dnf install -y ./zebra-scanner-corescanner-4.4.1-30.fc23.x86_64.rpm
sudo dnf install -y ./zebra-scanner-devel-4.4.1-30.fc23.x86_64.rpm
```
Добавляем пользователя super в дополнительные группы
```
sudo usermod -a -G video super && sudo usermod -a -G dialout super
```
Добавляем репозитарий REMI для установки dlib
```
sudo dnf install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm
```
Установка dlib
```
sudo dnf install -y dlib-devel
```
Загрузка пакетов opencv. Пакеты надо загрузить отдельно, потому что с opencv в системном репозитарии не запускаются программы из-за разных версий protobuf в opencv и в системе.
```
mkdir -p $HOME/distib/rpm && cd $HOME/distib/rpm
wget https://vault.centos.org/8.5.2111/AppStream/x86_64/os/Packages/opencv-core-3.4.6-6.el8.x86_64.rpm
wget https://vault.centos.org/8.5.2111/AppStream/x86_64/os/Packages/opencv-contrib-3.4.6-6.el8.x86_64.rpm
wget https://vault.centos.org/8.5.2111/PowerTools/x86_64/os/Packages/opencv-3.4.6-6.el8.x86_64.rpm
wget https://vault.centos.org/8.5.2111/PowerTools/x86_64/os/Packages/opencv-devel-3.4.6-6.el8.x86_64.rpm
sudo dnf install ./opencv-core-3.4.6-6.el8.x86_64.rpm
sudo dnf install ./opencv-contrib-3.4.6-6.el8.x86_64.rpm
sudo dnf install ./opencv-3.4.6-6.el8.x86_64.rpm
sudo dnf install ./opencv-devel-3.4.6-6.el8.x86_64.rpm
```
Добавляем arm в /etc/hosts
```
sudo bash -c 'echo 172.16.104.36   arm  >>/etc/hosts'
```
Настраиваем автомонтирование по nfs /project -> arm:/project
```
sudo mkdir /project
sudo chown super:super /project
```
```
sudo bash -c 'cat > /etc/systemd/system/project.mount << EOF
[Unit]
Description=mount folder from arm

[Mount]
What=arm:/project
Where=/project
Type=nfs
Options=defaults
TimeoutSec=5

[Install]
WantedBy=remote-fs.target
EOF
'
```
```
sudo bash -c 'cat > /etc/systemd/system/project.automount << EOF
[Unit]
Description=/project automount for arm:/project

[Automount]
Where=/project
TimeoutIdleSec=0

[Install]
WantedBy=remote-fs.target
EOF
'
```
```
sudo dnf install -y nfs-utils nfs4-acl-tools
sudo systemctl enable project.automount
sudo systemctl start project.automount
```
Установка дополнений для gstreamer
```
sudo dnf install -y gstreamer1-libav gstreamer1-plugins-bad-free gstreamer1-plugins-base gstreamer1-plugins-good gstreamer1-plugins-good-gtk gstreamer1-plugins-ugly-free gstreamer1-vaapi
```
Настройка сервера времени
```
sudo sed -i 's/pool 2.rocky.pool.ntp.org/server arm/g' /etc/chrony.conf
sudo systemctl restart chronyd.service
```
Добавление правила udev для считывателя отпечатков пальцев чтобы были права на запись и чтение
```
sudo bash -c 'cat > /etc/udev/rules.d/89-slk20r.rules << EOF
#ZKTECO SLK20R
SUBSYSTEM=="usb", ACTION=="add", ENV{DEVTYPE}=="usb_device", ATTR{idVendor}=="1b55", ATTR{idProduct}=="0120", MODE:="0666"
EOF
'
```
```
sudo udevadm control --reload-rules && sudo udevadm trigger
```
Отключение firewall
```
sudo systemctl disable firewalld && sudo systemctl stop firewalld 
```
Установка брокера MQTT mosquitto
```
sudo dnf install -y mosquitto && sudo systemctl enable --now mosquitto.service
```

