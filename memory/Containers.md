# Containers <!-- Metadata: type: Outline; created: 2021-03-29 19:50:25; reads: 23; read: 2021-04-02 21:22:44; revision: 21; modified: 2021-04-02 21:22:44; importance: 0/5; urgency: 0/5; -->
# Podman containers sample commands <!-- Metadata: type: Note; tags: podman,container,docker; created: 2021-03-29 19:50:25; reads: 18; read: 2021-04-02 21:22:44; revision: 11; modified: 2021-04-02 21:22:44; -->
`podman login container-registry.oracle.com` Login to oracle registry  
`podman pull oraclelinux:8` pull fresh oracle linux 8  
`podman inspect oraclelinux:8` inspect image  
`podman create --name oracle oraclelinux:8` create Oracle linux container  
`podman run --rm oraclelinux:8 cat /etc/oracle-release` run and destroy a single command container  
`podman run --name=oracleshell -it oraclelinux:8 /bin/bash` The -i flag makes the container interactive and -t connects the local terminal to the container  
`podman start -ai oracleshell` To restart the container and connect to it again  
`podman container list -a` list all containers  
`podman container rm 10ba3267cb9e` remove containers  
`podman run --name=oracledaemon -p 8000:80 oraclelinux:8 su -c 'dnf -y install httpd && apachectl -D FOREGROUND'` install & run apache  
`podman pod create --name oraclepod` create pod  
`podman run -d --pod oraclepod nginx` start container in the podman  
`podman generate systemd oraclepod` podman generate systemd unit for pod or container  
`sudo loginctl enable-linger gleb` enable running processes without logging in  
`mkdir -p $HOME/.config/systemd/user/` create systemd unit user directory  
`cd $HOME/.config/systemd/user/ && podman generate systemd --files oraclepod` generate systemd unit files for pod  
`systemctl --user start pod-45258c85e5066579017fd9e991a329bbfc8c0e15386e1ca5f9ca32ada8cae65c.service` start generated pod service. Also will start generated containers services  
`podman generate systemd --files --restart-policy=always -t 1 oraclepod` создание systemd unit файлов с авторестартом, принудительное завершение контейнера через 1 сек  
`buildah bud -t imagename .` построить образ из Dockerfile. Use the -f filename option if the file does not use the default Dockerfile filename.  
`podman container commit oracledaemon oracledaemon2` создать новый образ из контейнера  
`mkdir /tmp/1 && podman push oracledaemon2 dir:/tmp/1` отправить образ в локальный каталог  
`podman push oracledaemon2 oci-archive:/tmp/1.tar:ora2:ora2` отправить образ в tar файл

---
    $ cat Dockerfile
    FROM oraclelinux:8
    WORKDIR /opt  
    RUN dnf -y module install python38 &&
    dnf clean all
    ENTRYPOINT /bin/python3 -m http.server 8000

`podman build --tag oraclelinux:pyhttp .` построить образ из Dockerfile  
`mkdir $HOME/src/t2/data`  
`podman run -d -p 8080:8000 --name webapp1 -v $HOME/src/t2/data:/opt oraclelinux:pyhttp` run container from created image, volume mapping  
`for i in {1..10}; do >$HOME/src/t2/data/file${i}; done` создать файлы  
`curl localhost:8080` посмотреть список файлов  
`podman ps -a` список контейнеров  
`podman stop webapp1 && podman rm webapp1` уничтожить контейнер  


---
`podman volume create my_vol` создать том  
`podman volume ls` список томов  
`podman run -it -v my_vol:/data --name box1 oraclelinux:8` запустить контейнер, смонтировать то и подключиться к консоли  

