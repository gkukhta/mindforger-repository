# Windows <!-- Metadata: type: Outline; created: 2021-04-30 01:22:00; reads: 85; read: 2021-05-02 12:35:41; revision: 85; modified: 2021-05-02 12:35:41; importance: 0/5; urgency: 0/5; -->
# Добавление VLAN в Windows 10 <!-- Metadata: type: Note; created: 2021-04-30 01:22:00; reads: 60; read: 2021-05-02 12:35:41; revision: 41; modified: 2021-05-02 12:35:41; -->
Все команды по настройке из этой инструкции необходимо выполнять из **powershell с правами администратора локального компьютера**. Рассмотрим добавление VLAN 90 имеющемуся подключению. В к.106 надо подключиться к одному из следующих портов: 02,04,05,06,07,08,09,10,11,13,14,15,16,17,18,19,20. На этих портах VLAN 90 передается с тэгом.

Имеется следующая конфигурация интерфейса, полученная по DHCP:
```
PS C:\> ipconfig

Настройка протокола IP для Windows

Адаптер Ethernet Ethernet:

   DNS-суффикс подключения . . . . . : ctr
   Локальный IPv6-адрес канала . . . : fe80::f543:7381:3ee:c41e%15
   IPv4-адрес. . . . . . . . . . . . : 172.16.104.52
   Маска подсети . . . . . . . . . . : 255.255.255.0
   Основной шлюз. . . . . . . . . : 172.16.104.1
```
Необходимо включить Hyper-V, чтобы появилась возможность создавать виртуальные коммутаторы. У виртуального коммутатора, который мы создадим, будет ip-адрес нашего основного подключения, без VlanID. В виртуальном коммутаторе создадим виртуальный интерфейс с VlanID 90. 

Включаем Hyper-V в Windows:
```
PS C:\> Enable-WindowsOptionalFeature -Online -FeatureName:Microsoft-Hyper-V -All
Вы хотите перезапустить компьютер сейчас для завершения этой операции?
[Y] Yes  [N] No  [?] Справка (значением по умолчанию является "Y"):
```
**Перезагружаемся.**

Смотрим, на каком физическом интерфейсе создавать виртуальный коммутатор. В Нашем случае это "Ethernet".
```
PS C:\> Get-NetAdapter -Physical

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Ethernet                  Red Hat VirtIO Ethernet Adapter               6 Up           9E-4D-2B-D0-62-BC        10 Gbps
```
Создаём виртуальный коммутатор.
```
PS C:\> New-VMSwitch -Name "VLAN Switch" -NetAdapterName "Ethernet"

Name        SwitchType NetAdapterInterfaceDescription
----        ---------- ------------------------------
VLAN Switch External   Red Hat VirtIO Ethernet Adapter
```
Теперь добавляем в созданный виртуальный коммутатор виртуальный сетевой интерфейс VLAN90.
```
PS C:\> Add-VMNetworkAdapter -ManagementOS -Name VLAN90 -StaticMacAddress (Get-NetAdapter -Physical | Select -First 1 | %{$_.MacAddress}) -SwitchName "VLAN Switch"
```
Теперь добавляем VlanID 90 созданному сетевому интерфейсу.
```
PS C:\> Set-VMNetworkAdapterVlan -ManagementOS -VMNetworkAdapterName VLAN90 -Access -VlanID 90
```
**Перезагружаемся**
```
PS C:\> Restart-Computer
```
Отключаем large send offload на наших сетевых адаптерах:
```
PS C:\> Get-NetAdapter

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
vEthernet (Default Swi... Hyper-V Virtual Ethernet Adapter             21 Up           F8-15-B6-5D-1B-EC        10 Gbps
vEthernet (VLAN90)        Hyper-V Virtual Ethernet Adapter #3          15 Up           9E-4D-2B-D0-62-BC        10 Gbps
vEthernet (VLAN Switch)   Hyper-V Virtual Ethernet Adapter #2          13 Up           9E-4D-2B-D0-62-BC        10 Gbps
Ethernet                  Red Hat VirtIO Ethernet Adapter               5 Up           9E-4D-2B-D0-62-BC        10 Gbps

PS C:\> Disable-NetAdapterLso -Name "vEthernet (VLAN90)" -IPv4 -IPv6
PS C:\> Disable-NetAdapterLso -Name "vEthernet (VLAN Switch)" -IPv4 -IPv6
PS C:\> Disable-NetAdapterLso -Name "Ethernet" -IPv4 -IPv6

```
Отключаем ненужный нам IPv6 на сетевых адаптерах:
```
PS C:\> Disable-NetAdapterBinding -Name "vEthernet (VLAN90)" -ComponentID ms_tcpip6
PS C:\> Disable-NetAdapterBinding -Name "vEthernet (VLAN Switch)" -ComponentID ms_tcpip6
```
Добавляем постоянный маршрут в сеть 10.0.0.0/8
```
PS C:\> route -p ADD 10.0.0.0 mask 255.0.0.0 10.0.90.1
 ОК
```
Для каждого сетевого интерфейса мы получаем от своего DHCP сервера собственный маршрут по умолчанию:
```
PS C:\> route -4 print
...
0.0.0.0          0.0.0.0     172.16.104.1    172.16.104.52     15
0.0.0.0          0.0.0.0        10.0.90.1       10.0.90.74     15
...
```
Чтобы маршрут по умолчанию из сети 172.16.104.0/24 был предпочтительным, увеличим значение метрики интерфейса VLAN90.
```
PS C:\> Get-NetIPInterface

ifIndex InterfaceAlias                  AddressFamily NlMtu(Bytes) InterfaceMetric Dhcp     ConnectionState PolicyStore
------- --------------                  ------------- ------------ --------------- ----     --------------- -----------
21      vEthernet (Default Switch)      IPv6                  1500            5000 Enabled  Connected       ActiveStore
1       Loopback Pseudo-Interface 1     IPv6            4294967295              75 Disabled Connected       ActiveStore
18      vEthernet (VLAN90)              IPv4                  1500              15 Enabled  Connected       ActiveStore
19      vEthernet (VLAN Switch)         IPv4                  1500              15 Enabled  Connected       ActiveStore
21      vEthernet (Default Switch)      IPv4                  1500            5000 Disabled Connected       ActiveStore
1       Loopback Pseudo-Interface 1     IPv4            4294967295              75 Disabled Connected       ActiveStore

PS C:\> Set-NetIPInterface -InterfaceAlias "vEthernet (VLAN90)" -InterfaceMetric 30
```

**Настройка завершена.**

Проверяем что всё работает.
```
PS C:\> ping gitlab.ctr

Обмен пакетами с gitlab.ctr [172.16.104.8] с 32 байтами данных:
Ответ от 172.16.104.8: число байт=32 время=10мс TTL=64
Ответ от 172.16.104.8: число байт=32 время<1мс TTL=64
Ответ от 172.16.104.8: число байт=32 время<1мс TTL=64
Ответ от 172.16.104.8: число байт=32 время<1мс TTL=64

Статистика Ping для 172.16.104.8:
    Пакетов: отправлено = 4, получено = 4, потеряно = 0
    (0% потерь)
Приблизительное время приема-передачи в мс:
    Минимальное = 0мсек, Максимальное = 10 мсек, Среднее = 2 мсек
PS C:\> ping gitlab.fintech.ru

Обмен пакетами с gitlab.fintech.ru [10.0.90.21] с 32 байтами данных:
Ответ от 10.0.90.21: число байт=32 время=2мс TTL=64
Ответ от 10.0.90.21: число байт=32 время<1мс TTL=64
Ответ от 10.0.90.21: число байт=32 время<1мс TTL=64
Ответ от 10.0.90.21: число байт=32 время<1мс TTL=64

Статистика Ping для 10.0.90.21:
    Пакетов: отправлено = 4, получено = 4, потеряно = 0
    (0% потерь)
Приблизительное время приема-передачи в мс:
    Минимальное = 0мсек, Максимальное = 2 мсек, Среднее = 0 мсек
```
Для информации приведу результаты тестирования скорости доступа к компьютерам в сетях 10.0.90.0/24 и 172.16.104.0/24 с подключенным по этой инструкции VLAN 90 на виртуалке с Windows 10, а также результат скорости доступа к интернету speedtest. Скорость интернета командой speedtest измерена с подключением через шлюз 172.16.104.1. Имя этой виртуалки, DESKTOP-5DCUHCI.fintech.ru, вход по RDP открыт для своих.
```
C:\bin>iperf3.exe -c 172.16.104.117
Connecting to host 172.16.104.117, port 5201
[  4] local 172.16.104.52 port 50471 connected to 172.16.104.117 port 5201
[ ID] Interval           Transfer     Bandwidth
[  4]   0.00-1.00   sec  67.9 MBytes   569 Mbits/sec
[  4]   1.00-2.00   sec   100 MBytes   842 Mbits/sec
[  4]   2.00-3.00   sec  99.1 MBytes   832 Mbits/sec
[  4]   3.00-4.00   sec   103 MBytes   862 Mbits/sec
[  4]   4.00-5.00   sec   100 MBytes   843 Mbits/sec
[  4]   5.00-6.00   sec   101 MBytes   848 Mbits/sec
[  4]   6.00-7.00   sec   100 MBytes   840 Mbits/sec
[  4]   7.00-8.00   sec   101 MBytes   844 Mbits/sec
[  4]   8.00-9.00   sec  98.1 MBytes   823 Mbits/sec
[  4]   9.00-10.00  sec  98.9 MBytes   829 Mbits/sec
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth
[  4]   0.00-10.00  sec   970 MBytes   813 Mbits/sec                  sender
[  4]   0.00-10.00  sec   969 MBytes   813 Mbits/sec                  receiver

iperf Done.

C:\bin>iperf3.exe -c 10.0.90.49
Connecting to host 10.0.90.49, port 5201
[  4] local 10.0.90.74 port 50473 connected to 10.0.90.49 port 5201
[ ID] Interval           Transfer     Bandwidth
[  4]   0.00-1.00   sec  36.6 MBytes   307 Mbits/sec
[  4]   1.00-2.00   sec  95.9 MBytes   804 Mbits/sec
[  4]   2.00-3.00   sec  94.2 MBytes   790 Mbits/sec
[  4]   3.00-4.00   sec   100 MBytes   843 Mbits/sec
[  4]   4.00-5.00   sec  96.6 MBytes   810 Mbits/sec
[  4]   5.00-6.00   sec   102 MBytes   852 Mbits/sec
[  4]   6.00-7.00   sec  99.2 MBytes   833 Mbits/sec
[  4]   7.00-8.00   sec  94.8 MBytes   794 Mbits/sec
[  4]   8.00-9.00   sec   102 MBytes   858 Mbits/sec
[  4]   9.00-10.00  sec  99.9 MBytes   837 Mbits/sec
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth
[  4]   0.00-10.00  sec   921 MBytes   773 Mbits/sec                  sender
[  4]   0.00-10.00  sec   921 MBytes   773 Mbits/sec                  receiver

iperf Done.

C:\bin>speedtest.exe

   Speedtest by Ookla

     Server: Telecom Center - Moscow (id = 17091)
        ISP: ER-Telecom
    Latency:     1.53 ms   (0.19 ms jitter)
   Download:   731.17 Mbps (data used: 330.7 MB)
     Upload:   720.96 Mbps (data used: 1.2 GB)
Packet Loss: Not available.
 Result URL: https://www.speedtest.net/result/c/39931ae0-5750-4797-8b8d-426ae559a008
```
---
Команды удаления VLAN можно посмотреть [тут](remove-vlan.md).
