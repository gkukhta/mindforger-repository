# pbs fileserver <!-- Metadata: type: Outline; created: 2021-05-16 04:35:24; reads: 15; read: 2021-05-17 15:32:22; revision: 15; modified: 2021-05-17 15:32:22; importance: 0/5; urgency: 0/5; -->
Все команды по настройке из этой инструкции необходимо выполнять из **powershell с правами администратора локального компьютера**. Имя файл-сервера: **pbs**.
Доступ к серверу pbs из сети 10.0.0.0/8 заблокирован для безопасности и из-за **очень** медленного шлюза.
Инструкцию по добавлению VLAN в Windows 10 можно прочитать [здесь](Windows.md).
Для того, чтобы сервер появился в списке в проводнике Windows, выполните такую команду:
```
PS C:\> Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters" -Name "AllowInsecureGuestAuth" -Type DWord -Value 1
```
Для изменения своего пароля на файл-сервере samba подключитесь к нему со своим логином по ssh. В Windows 10 выпуска 2018 года и более поздних есть штатный ssh.
```
C:\>ssh smbtest@pbs
The authenticity of host 'pbs (172.16.104.229)' can't be established.
ECDSA key fingerprint is SHA256:lG3mbnpHGe5y0/RcgBoRgwwipPd0Y6Z8b9Ayu89heaE.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'pbs,172.16.104.229' (ECDSA) to the list of known hosts.
Old SMB password:
New SMB password:
Retype new SMB password:
Password changed for user smbtest
Connection to pbs closed.
```
Сейчас на файл-сервере доступно 3 ресурса:

1. Ресурс с именем пользователя, он же **homes**. Личный ресурс пользователя. Больше недоступен никому.
2. Ресурс **pub** - все могут создавать, изменять, удалять, читать свои и чужие файлы.
3. Ресурс **distrib** - все могут создавать файлы. Все могут читать свои и чужие файлы. Только владелец может изменять и удалять файлы.
# Note <!-- Metadata: type: Note; created: 2021-05-16 04:35:24; reads: 1; read: 2021-05-16 04:35:24; revision: 1; modified: 2021-05-16 04:35:24; -->

