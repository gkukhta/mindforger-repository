Команда удаления интерфейса VLAN90:
```
PS C:\> Remove-VMNetworkAdapter -ManagementOS -VMNetworkAdapterName VLAN90
```
Команда удаления виртуального коммутатора:
```
PS C:\> Remove-VMSwitch -Name "VLAN Switch"                                                                                                                                                            
Подтверждение
Вы действительно хотите удалить виртуальный коммутатор "VLAN Switch"?
[Y] Да - Y  [A] Да для всех - A  [N] Нет - N  [L] Нет для всех - L  [S] Приостановить - S  [?] Справка (значением по умолчанию является "Y"):
```
