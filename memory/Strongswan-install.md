# Strongswan install <!-- Metadata: type: Outline; created: 2021-04-18 16:06:40; reads: 25; read: 2021-04-19 02:29:42; revision: 25; modified: 2021-04-19 02:29:42; importance: 0/5; urgency: 0/5; -->
```
sudo apt install strongswan strongswan-pki \
libcharon-extra-plugins libcharon-extauth-plugins \
libstrongswan-extra-plugins
```
`mkdir -p ~/pki/{cacerts,certs,private}`

`pki --gen --type ecdsa --size 384 --outform pem > ~/pki/private/ca-key.pem`
```
pki --self --ca --lifetime 10000 --in ~/pki/private/ca-key.pem \
--type ecdsa --dn "CN=CTR VPN root CA" --outform pem \
> ~/pki/cacerts/ca-cert.pem
```
`pki --gen --type ecdsa --size 384 --outform pem > ~/pki/private/server-key.pem`

`chmod 400 ~/pki/private/*`
```
pki --pub --in ~/pki/private/server-key.pem --type ecdsa | \
pki --issue --lifetime 9000 --cacert ~/pki/cacerts/ca-cert.pem \
--cakey ~/pki/private/ca-key.pem --dn "CN=86.62.80.170" \
--san @86.62.80.170 --san 86.62.80.170 --flag serverAuth \
--flag ikeIntermediate --outform pem >~/pki/certs/server-cert.pem
```
`sudo cp -r ~/pki/* /etc/ipsec.d/`

File /etc/ipsec.conf
```
config setup
    charondebug="dmn 0, mgr 0, ike 0, chd 0, job 0, cfg 0, knl 0, net 0, asn 0, enc 0, lib 0, esp 0, tls 0, tnc 0, imc 0, imv 0, pts 0"
    uniqueids=never

conn %default
    auto=add
    compress=no
    type=tunnel
    keyexchange=ikev2
    fragmentation=yes
    forceencaps=yes
    dpdaction=clear
    dpddelay=300s
    rekey=no
    left=%any
    leftid=86.62.80.170
    leftcert=server-cert.pem
    leftsendcert=always    
    right=%any    
    rightauth=eap-mschapv2
    rightsendcert=never
    eap_identity=%identity
    ike=chacha20poly1305-sha512-curve25519-prfsha512,aes256gcm16-sha384-prfsha384-ecp384,aes256-sha1-modp1024,aes128-sha1-modp1024,3des-sha1-modp1024!
    esp=chacha20poly1305-sha512,aes256gcm16-ecp384,aes256-sha256,aes256-sha1,3des-sha1!

conn ctr-vpn
    leftsubnet=172.16.104.0/24,172.17.0.0/16
    rightsourceip=172.16.101.0/24
    rightid=*@ctr
    rightdns=172.16.104.1
    
conn ivanilov-vpn
    leftsubnet=172.16.104.221/32,172.16.104.222/32,172.16.104.223/32,172.17.128.0/24
    rightsourceip=172.16.102.0/24
    rightid=*@ivanilov
```
File /etc/ipsec.secrets
```
: ECDSA "server-key.pem"
kukhta@ctr : EAP "mypassword"
```
`chmod og-r /etc/ipsec.secrets`

`sudo systemctl restart strongswan-starter`

`sudo ufw allow 500,4500/udp`

/etc/ufw/before.rules добавляем cекцию mangle, добавляем секцию -A ufw-before-forward после *filter
```
*nat

:PREROUTING ACCEPT [0:0]
-A PREROUTING -i ens19 -d 86.62.80.170 -p tcp --dport 3393 -j DNAT --to-destination 192.168.2.2:80
-A PREROUTING -i ens19 -d 86.62.80.170 -p tcp --dport 34567 -j DNAT --to-destination 192.168.2.2:34567
-A PREROUTING -i ens19 -d 86.62.80.170 -p udp --dport 34567 -j DNAT --to-destination 192.168.2.2:34567
-A PREROUTING -i ens19 -d 86.62.80.170 -p tcp --dport 23654 -j DNAT --to-destination 172.16.104.116:22
-A PREROUTING -i ens19 -d 86.62.80.170 -p tcp --dport 31415 -j DNAT --to-destination 172.16.104.116:443
#-A PREROUTING -i ens19 -d 86.62.80.170 -p tcp --dport 2222 -j DNAT --to-destination 172.16.104.249:22

:POSTROUTING ACCEPT [0:0]

# Forward traffic from eth1 through eth0.
-A POSTROUTING -s 172.16.104.0/24 -o ens19 -j MASQUERADE
-A POSTROUTING -s 192.168.2.0/24 -o ens19 -j MASQUERADE
-A POSTROUTING -s 10.1.1.54 -o ens19 -j MASQUERADE

# don't delete the 'COMMIT' line or these nat table rules won't be processed
COMMIT

*mangle
-A FORWARD --match policy --pol ipsec --dir in -s 172.16.101.0/24 -o eth0 -p tcp -m tcp --tcp-flags SYN,RST SYN -m tcpmss --mss 1361:1536 -j TCPMSS --set-mss 1360
-A FORWARD --match policy --pol ipsec --dir in -s 172.16.102.0/24 -o eth0 -p tcp -m tcp --tcp-flags SYN,RST SYN -m tcpmss --mss 1361:1536 -j TCPMSS --set-mss 1360

COMMIT

# Don't delete these required lines, otherwise there will be errors
*filter
:ufw-before-input - [0:0]
:ufw-before-output - [0:0]
:ufw-before-forward - [0:0]
:ufw-not-local - [0:0]
# End required lines

-A ufw-before-forward --match policy --pol ipsec --dir in --proto esp -s 172.16.101.0/24 -j ACCEPT
-A ufw-before-forward --match policy --pol ipsec --dir out --proto esp -d 172.16.101.0/24 -j ACCEPT
-A ufw-before-forward --match policy --pol ipsec --dir in --proto esp -s 172.16.102.0/24 -j ACCEPT
-A ufw-before-forward --match policy --pol ipsec --dir out --proto esp -d 172.16.102.0/24 -j ACCEPT
```
/etc/ufw/sysctl.conf
```
net/ipv4/ip_forward=1
. . .
net/ipv4/conf/all/accept_redirects=0
net/ipv4/conf/all/send_redirects=0
```
Client /etc/ipsec.conf
```
config setup

conn ikev2-rw
    right=86.62.80.170
    # This should match the `leftid` value on your server's configuration
    rightid=86.62.80.170
    rightsubnet=0.0.0.0/0
    rightauth=pubkey
    leftsourceip=%config
    leftid=kukhta@ctr
    leftauth=eap-mschapv2
    eap_identity=%identity
    auto=starter
```

Client /etc/ipsec.secrets
```
# This file holds shared secrets or RSA private keys for authentication.

# RSA private key for this host, authenticating it to any other host
# which knows the public part.
kukhta@ctr  : EAP "mypassword"
```
# Note <!-- Metadata: type: Note; created: 2021-04-18 16:06:40; reads: 4; read: 2021-04-18 19:54:48; revision: 1; modified: 2021-04-18 16:06:40; -->

