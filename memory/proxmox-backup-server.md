# proxmox backup server <!-- Metadata: type: Outline; tags: pbs,proxmox,backup; created: 2024-12-23 18:07:02; reads: 5; read: 2024-12-23 18:10:52; revision: 5; modified: 2024-12-23 18:10:52; importance: 0/5; urgency: 0/5; -->
PBS backup physical workstation service
```
[Unit]
Description=Proxmox Backup Client Service
After=network.target

[Service]
Type=oneshot
Environment="PBS_PASSWORD=551eba58-dee3-430f-ae13-dc42394198d9"
Environment="PBS_REPOSITORY=root@pam!tk3@pbs:backupspace1"
Environment="PBS_FINGERPRINT=53:e9:c0:f9:b7:69:03:8d:3c:a2:5d:86:78:1b:03:0a:6f:9b:41:d4:66:fe:e9:a0:ad:a8:e4:ae:9d:0d:e0:1f"
Environment="PBS_ENCRYPTION_PASSWORD_FILE=/etc/pbspwd"
ExecStart=/usr/bin/proxmox-backup-client backup \
          %H.pxar:/ \
          %H-home.pxar:/home \
          --skip-lost-and-found \
          --keyfile /etc/pbsgleb.key \
          --exclude /gleb/distrib/
```
PBS backup timer
```
[Unit]
Description=Run Proxmox Backup Client Service for / daily at 23:09:13

[Timer]
OnCalendar=*-*-* 23:09:13
Persistent=true

[Install]
WantedBy=timers.target
```
# Note <!-- Metadata: type: Note; created: 2024-12-23 18:07:02; reads: 1; read: 2024-12-23 18:07:02; revision: 1; modified: 2024-12-23 18:07:02; -->

