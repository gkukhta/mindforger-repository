# Debian 10 proxmox persistent names <!-- Metadata: type: Outline; tags: proxmox,debian,network; created: 2021-04-02 21:23:49; reads: 7; read: 2021-04-05 12:58:24; revision: 7; modified: 2021-04-05 12:58:24; importance: 0/5; urgency: 0/5; -->
`cat /usr/lib/systemd/network/99-default.link`
```
#  SPDX-License-Identifier: LGPL-2.1+
#
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

[Link]
#NamePolicy=keep kernel database onboard slot path
NamePolicy=path
MACAddressPolicy=persistent
```
`cat /etc/network/interfaces`
```
auto lo
iface lo inet loopback

auto enp130s0f0
iface enp130s0f0 inet static
	address 192.168.120.1/24
#Ceph

iface enp130s0f1 inet manual

auto enp6s0f0
iface enp6s0f0 inet manual

iface enp6s0f1 inet manual

auto enp7s0f0
iface enp7s0f0 inet manual

iface enp7s0f1 inet manual

auto bond0
iface bond0 inet manual
	ovs_bonds enp6s0f0 enp7s0f0
	ovs_type OVSBond
	ovs_bridge vmbr0
	ovs_options lacp=active bond_mode=balance-slb

auto vmbr0
iface vmbr0 inet static
	address 172.16.104.21/24
	gateway 172.16.104.1
	ovs_type OVSBridge
	ovs_ports bond0
```
# Note <!-- Metadata: type: Note; created: 2021-04-02 21:23:49; reads: 1; read: 2021-04-02 21:23:49; revision: 1; modified: 2021-04-02 21:23:49; -->

