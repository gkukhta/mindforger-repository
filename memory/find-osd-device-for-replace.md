# Ceph <!-- Metadata: type: Outline; tags: osd,ceph,hdd,replace; created: 2021-04-09 20:22:53; reads: 25; read: 2021-04-22 03:56:14; revision: 25; modified: 2021-04-22 03:56:14; importance: 0/5; urgency: 0/5; -->
`root@asr2:~# ceph-volume lvm list`
```
====== osd.14 ======

  [block]       /dev/ceph-36c28faf-a8e2-4c69-a68d-7b7d340cef01/osd-block-37fdbdd9-d56b-4757-a5e1-2d187ce64e87

      block device              /dev/ceph-36c28faf-a8e2-4c69-a68d-7b7d340cef01/osd-block-37fdbdd9-d56b-4757-a5e1-2d187ce64e87
      block uuid                csOnQf-XwDQ-Kwgv-xu7e-WMxN-HC1t-Povrpl
      cephx lockbox secret      
      cluster fsid              8fc87072-5946-466f-a10a-6fa9bd6fa925
      cluster name              ceph
      crush device class        None
      encrypted                 0
      osd fsid                  37fdbdd9-d56b-4757-a5e1-2d187ce64e87
      osd id                    14
      osdspec affinity          
      type                      block
      vdo                       0
      devices                   /dev/sdb
...
```
Дальше командами или через GUI
`ceph osd out osd.<id>`

`ceph osd safe-to-destroy osd.<id>`

`systemctl stop ceph-osd@<id>.service`

`pveceph osd destroy <id>`

`ledctl locate=/dev/sdb`

Поменять диск.

`ledctl locate_off=/dev/sdb`

`ceph-volume lvm zap /dev/sd[X] --destroy`

`pveceph osd create /dev/sd[X]`


# Create ceph monitor <!-- Metadata: type: Note; tags: ceph,monitor; created: 2021-04-21 03:34:59; reads: 4; read: 2021-04-21 03:35:56; revision: 3; modified: 2021-04-21 03:35:56; -->
```
root@asr3:~# ceph mon getmap -o tmp/map
got monmap epoch 5
root@asr3:~# ceph auth get mon. -o tmp/key
exported keyring for mon.
root@asr3:~# rm -rf /var/lib/ceph/mon/ceph-asr3/
root@asr3:~# systemctl stop ceph-mon.target
root@asr3:~# systemctl stop ceph-mon@asr3.service
root@asr3:~# ceph-mon -i asr3 --mkfs --monmap tmp/map --keyring tmp/key
root@asr3:~# ceph-mon -i asr3 --public-network 192.168.121.0/24
root@asr3:~# ss -tulpn | grep '3300\|6789'
tcp     LISTEN   0        512        192.168.121.3:3300           0.0.0.0:*      users:(("ceph-mon",pid=2491129,fd=27))                                         
tcp     LISTEN   0        512        192.168.121.3:6789           0.0.0.0:*      users:(("ceph-mon",pid=2491129,fd=28))
root@asr3:~# ceph mon dump
dumped monmap epoch 5
epoch 5
fsid 8fc87072-5946-466f-a10a-6fa9bd6fa925
last_changed 2021-04-21T00:46:55.557939+0300
created 2021-04-06T16:06:42.329089+0300
min_mon_release 15 (octopus)
0: [v2:192.168.121.1:3300/0,v1:192.168.121.1:6789/0] mon.asr1
1: [v2:192.168.121.2:3300/0,v1:192.168.121.2:6789/0] mon.asr2
2: [v2:192.168.121.4:3300/0,v1:192.168.121.4:6789/0] mon.asr4
root@asr3:~# cd /etc/systemd/system/ceph-mon.target.wants
root@asr3:/etc/systemd/system/ceph-mon.target.wants# ln -s /lib/systemd/system/ceph-mon@.service ceph-mon@asr3.service
```
# Replace OSD <!-- Metadata: type: Note; tags: ceph,osd; created: 2021-04-21 03:33:47; reads: 4; read: 2021-04-21 03:33:56; revision: 2; modified: 2021-04-21 03:33:56; -->
`root@asr2:~# ceph-volume lvm list`
```
====== osd.14 ======

  [block]       /dev/ceph-36c28faf-a8e2-4c69-a68d-7b7d340cef01/osd-block-37fdbdd9-d56b-4757-a5e1-2d187ce64e87

      block device              /dev/ceph-36c28faf-a8e2-4c69-a68d-7b7d340cef01/osd-block-37fdbdd9-d56b-4757-a5e1-2d187ce64e87
      block uuid                csOnQf-XwDQ-Kwgv-xu7e-WMxN-HC1t-Povrpl
      cephx lockbox secret      
      cluster fsid              8fc87072-5946-466f-a10a-6fa9bd6fa925
      cluster name              ceph
      crush device class        None
      encrypted                 0
      osd fsid                  37fdbdd9-d56b-4757-a5e1-2d187ce64e87
      osd id                    14
      osdspec affinity          
      type                      block
      vdo                       0
      devices                   /dev/sdb
...
```
Дальше командами или через GUI
`ceph osd out osd.<id>`

`ceph osd safe-to-destroy osd.<id>`

`systemctl stop ceph-osd@<id>.service`

`pveceph osd destroy <id>`

`ledctl locate=/dev/sdb`

Поменять диск.

`ledctl locate_off=/dev/sdb`

`ceph-volume lvm zap /dev/sd[X] --destroy`

`pveceph osd create /dev/sd[X]`



# Migration running cluster to the new IPs <!-- Metadata: type: Note; created: 2021-04-09 20:22:53; reads: 9; read: 2021-04-22 03:56:14; revision: 4; modified: 2021-04-22 03:56:14; -->
 # Migration running cluster to the new IPs

 ## Ceph Network overview

Ceph Network overview was done [in this article][Ceph Network Configuration Reference]. Please read it before you
continue with the current page!

  ![Ceph Network](pics/ceph_network.png)

 ## Setup an the goals
`stor0{1,2,3,4}-htz-nbg1` servers host so called *slow ceph storage*. Legacy setup was:
```
[global]
fsid = fd49607d-ce8f-45c7-bdc9-14edc00d72eb
public_network = 172.26.2.0/24
cluster_network = 172.26.2.0/24
mon_initial_members = stor01-htz-nbg1, stor02-htz-nbg1, stor03-htz-nbg1, stor04-htz-nbg1
mon_host = 172.26.2.251,172.26.2.250,172.26.2.249,172.26.2.248
auth_cluster_required = cephx
auth_service_required = cephx 
auth_client_required = cephx
mds standby replay = true
```

The goal was to migrate `public_network` from `172.26.2.0/24` to `192.168.1.0/24, 10.94.181.0/24`.
The network configuration is the next:

| Network Interface | stor01-htz-nbg1 | stor02-htz-nbg1 | stor03-htz-nbg1 | stor04-htz-nbg1 |
| ---               | ---             | ---             | ---             | ---             |          
| `eth0`            | a.b.c.251       | a.b.c.250       | a.b.c.249       | a.b.c.248       |
| `eth1`            | 192.168.1.251   | 192.168.1.250   | 192.168.1.249   | 192.168.1.248   |
| `eth1.14`         | 172.26.2.251    | 172.26.2.250    | 172.26.2.249    | 172.26.2.248    |
| `eth1.77`         | 10.94.181.251   | 10.94.181.250   | 10.94.181.249   | 10.94.181.248   |

 ## Migration Process

 ### Generating Monmap while running legacy configuration
Do not stop/reboot/change configs while doing these steps.
```
> ceph mon getmap -o ~/MAPS/mon.getmap
```

 ### Printing file
```
> monmaptool --print ~/MAPS/mon.getmap

monmaptool: monmap file ~/MAPS/mon.getmap
epoch 1
fsid fd49607d-ce8f-45c7-bdc9-14edc00d72eb
last_changed 2019-11-04 20:00:40.996888
created 2019-11-04 20:00:40.996888
min_mon_release 14 (nautilus)
0: [v2:172.26.2.248:3300/0,v1:172.26.2.248:6789/0] mon.stor04-htz-nbg1
1: [v2:172.26.2.249:3300/0,v1:172.26.2.249:6789/0] mon.stor03-htz-nbg1
2: [v2:172.26.2.250:3300/0,v1:172.26.2.250:6789/0] mon.stor02-htz-nbg1
3: [v2:172.26.2.251:3300/0,v1:172.26.2.251:6789/0] mon.stor01-htz-nbg1
```

 ### Dropping legacy IPs
```
monmaptool --rm stor04-htz-nbg1 ~/MAPS/mon.getmap
monmaptool --rm stor03-htz-nbg1 ~/MAPS/mon.getmap
monmaptool --rm stor02-htz-nbg1 ~/MAPS/mon.getmap
monmaptool --rm stor01-htz-nbg1 ~/MAPS/mon.getmap
```

 ### Adding new IPs
`--addv` seems to be non-documented parameter. It adds the monitors addresses using the new config string format.

```
monmaptool --addv stor04-htz-nbg1 "[v2:192.168.1.248:3300/0,v2:10.94.181.248:3300/0,v1:192.168.1.248:6789/0,v1:10.94.181.248:6789/0]" ~/MAPS/mon.getmap
monmaptool --addv stor03-htz-nbg1 "[v2:192.168.1.249:3300/0,v2:10.94.181.249:3300/0,v1:192.168.1.249:6789/0,v1:10.94.181.249:6789/0]" ~/MAPS/mon.getmap
monmaptool --addv stor02-htz-nbg1 "[v2:192.168.1.250:3300/0,v2:10.94.181.250:3300/0,v1:192.168.1.250:6789/0,v1:10.94.181.250:6789/0]" ~/MAPS/mon.getmap
monmaptool --addv stor01-htz-nbg1 "[v2:192.168.1.251:3300/0,v2:10.94.181.251:3300/0,v1:192.168.1.251:6789/0,v1:10.94.181.251:6789/0]" ~/MAPS/mon.getmap
```

 ### Printing file
```
> monmaptool --print ~/MAPS/mon.getmap

monmaptool: monmap file ~/MAPS/mon.getmap
epoch 1
fsid fd49607d-ce8f-45c7-bdc9-14edc00d72eb
last_changed 2019-11-04 20:00:40.996888
created 2019-11-04 20:00:40.996888
min_mon_release 14 (nautilus)
0: [v2:172.26.2.248:3300/0,v2:10.94.181.248:3300/0,v1:172.26.2.248:6789/0,v1:10.94.181.248:6789/0] mon.stor04-htz-nbg1
1: [v2:172.26.2.249:3300/0,v2:10.94.181.249:3300/0,v1:172.26.2.249:6789/0,v1:10.94.181.249:6789/0] mon.stor03-htz-nbg1
2: [v2:172.26.2.250:3300/0,v2:10.94.181.250:3300/0,v1:172.26.2.250:6789/0,v1:10.94.181.250:6789/0] mon.stor02-htz-nbg1
3: [v2:172.26.2.251:3300/0,v2:10.94.181.251:3300/0,v1:172.26.2.251:6789/0,v1:10.94.181.251:6789/0] mon.stor01-htz-nbg1
```

 ### Redistrubute the file across the nodes of the cluster
```bash
scp mon.getmap rsdeploy@stor01-htz-nbg1:
scp mon.getmap rsdeploy@stor02-htz-nbg1:
scp mon.getmap rsdeploy@stor03-htz-nbg1:
scp mon.getmap rsdeploy@stor04-htz-nbg1:
```

The file would be in `/home/rsdeploy/mon.getmap` path.

 ### Stop all the monitors
Stop the monitors and insert the new CFG at the every node which hosts the monitor ():

```bash
root@stor01-htz-nbg1:/home/rsdeploy# systemctl status ceph-mon@stor01-htz-nbg1
* ceph-mon@stor01-htz-nbg1.service - Ceph cluster monitor daemon
   Loaded: loaded (/lib/systemd/system/ceph-mon@.service; indirect; vendor preset: enabled)
   Active: active (running) since Tue 2019-12-03 01:08:10 CET; 16h ago
root@stor01-htz-nbg1:/home/rsdeploy# systemctl stop ceph-mon@stor01-htz-nbg1
root@stor01-htz-nbg1:/home/rsdeploy# ceph-mon --id stor01-htz-nbg1 --inject-monmap /home/rsdeploy/mon.getmap
```

Obviously, the IDs are different at the nodes.

 ### Edit `/etc/hosts`

```
172.26.2.251    stor01-htz-nbg1
172.26.2.250    stor02-htz-nbg1
172.26.2.249    stor03-htz-nbg1
172.26.2.248    stor04-htz-nbg1

192.168.1.251   stor01-htz-nbg1
192.168.1.250   stor02-htz-nbg1
192.168.1.249   stor03-htz-nbg1
192.168.1.248   stor04-htz-nbg1

10.94.181.251   stor01-htz-nbg1
10.94.181.250   stor02-htz-nbg1
10.94.181.249   stor03-htz-nbg1
10.94.181.248   stor04-htz-nbg1
```

 ### change the configs ceph.conf
```bash
cat /home/rsdeploy/CEPH-RS-HDD-STOR/ceph.conf

[global]
fsid = fd49607d-ce8f-45c7-bdc9-14edc00d72eb
public_network = 192.168.1.0/24, 10.94.181.0/24
cluster_network = 172.26.2.0/24
mon_initial_members = stor01-htz-nbg1, stor02-htz-nbg1, stor03-htz-nbg1, stor04-htz-nbg1
mon_host = 192.168.1.251,192.168.1.250,192.168.1.249,192.168.1.248
auth_cluster_required = cephx
auth_service_required = cephx
auth_client_required = cephx
mds standby replay = true
```

Do ceph deploy:
```bash
ceph-deploy --overwrite-conf config push stor0{1,2,3,4}-htz-nbg1
```

 ### Check that the monitor listens at the new interfaces
```bash
root@stor04-htz-nbg1:~# netstat -pantu | grep ceph-mon
tcp        0      0 10.94.181.248:3300      0.0.0.0:*               LISTEN      9155/ceph-mon
tcp        0      0 192.168.1.248:3300      0.0.0.0:*               LISTEN      9155/ceph-mon
tcp        0      0 10.94.181.248:6789      0.0.0.0:*               LISTEN      9155/ceph-mon
tcp        0      0 192.168.1.248:6789      0.0.0.0:*               LISTEN      9155/ceph-mon
tcp        0      0 172.26.2.248:51088      172.26.2.250:6882       ESTABLISHED 9155/ceph-mon
tcp        0      0 192.168.1.248:59114     192.168.1.251:3300      ESTABLISHED 9155/ceph-mon
tcp        0      0 192.168.1.248:41414     192.168.1.250:3300      ESTABLISHED 9155/ceph-mon
tcp        0      0 192.168.1.248:50592     192.168.1.249:3300      ESTABLISHED 9155/ceph-mon
```

 ### Restart Ceph Managers, Metadata services and OSDs
```bash
root@stor01-htz-nbg1:/etc# systemctl restart ceph-mgr@stor01-htz-nbg1
root@stor01-htz-nbg1:/etc# systemctl restart ceph-mds@stor01-htz-nbg1
root@stor01-htz-nbg1:/etc# systemctl restart ceph-osd@{0..9}
```

```bash
root@stor02-htz-nbg1:/etc# systemctl restart ceph-mgr@stor02-htz-nbg1
root@stor02-htz-nbg1:/etc# systemctl restart ceph-mds@stor02-htz-nbg1
root@stor02-htz-nbg1:~# systemctl restart ceph-osd@{10..19}
```

```bash
root@stor03-htz-nbg1:/etc# systemctl restart ceph-mgr@stor03-htz-nbg1
root@stor03-htz-nbg1:/etc# systemctl restart ceph-mds@stor03-htz-nbg1
root@stor03-htz-nbg1:~# systemctl restart ceph-osd@{20..29}
```

```bash
root@stor04-htz-nbg1:/etc# systemctl restart ceph-mgr@stor04-htz-nbg1
root@stor04-htz-nbg1:/etc# systemctl restart ceph-mds@stor04-htz-nbg1
root@stor04-htz-nbg1:~# systemctl restart ceph-osd@{30..39}
```
